package br.up.edu.exemenu2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class Exe02Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exe02);
    }

    public void somar(View v){

        //Carrega as caixas de texto...
        EditText caixaNumero1 = (EditText) findViewById(R.id.txtNumero1);
        EditText caixaNumero2 = (EditText) findViewById(R.id.txtNumero2);
        EditText caixaResultado = (EditText) findViewById(R.id.txtResultado);

        //Pega o texto que está nas caixas...
        String txtCaixa1 = caixaNumero1.getText().toString();
        String txtCaixa2 = caixaNumero2.getText().toString();

        //Converter os textos para números...
        double vlr1 = Double.parseDouble(txtCaixa1);
        double vlr2 = Double.parseDouble(txtCaixa2);

        //Realizar a soma...
        double resultado = vlr1 + vlr2;

        //Mostrar o resultado para o usuário...
        caixaResultado.setText(String.valueOf(resultado));

    }
}













