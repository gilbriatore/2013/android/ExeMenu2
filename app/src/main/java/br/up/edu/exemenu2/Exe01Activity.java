package br.up.edu.exemenu2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Exe01Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exe01);
    }


    public void verificar(View v){
        //Carrega a caixa de texto com o número.
        EditText caixaDeNumero = (EditText) findViewById(R.id.cxNumero);
        EditText caixaDeMensagem = (EditText) findViewById(R.id.cxResultado);

        //Pega o texto da caixa
        String textoDaCaixa = caixaDeNumero.getText().toString();

        //Faz a conversão do texto para número
        int numero = Integer.parseInt(textoDaCaixa);

        if (numero > 10){
            //Mostrar a mensagem de que é maior do que 10.
            caixaDeMensagem.setText("É maior do que 10!");
        } else {
            //Mostrar a mensagem de que é menor ou igual a 10.
            caixaDeMensagem.setText("É menor ou igual a 10!");
        }
    }
}
